# -*- coding: utf-8 -*-
import ConfigParser
import re
import requests
import imdb

def existInBd(titulo):
    with open(db, 'r') as database:
        for line in database:
            if titulo in line:
                return True
    return False

def writeInBd(titulo, rating):
    f = open(db, 'a')
    f.write(titulo + "#" + str(rating) + "\n")
    f.close

#path_config = "/home/rasp/scripts/"
path_config = "./"

pelis_ini = path_config + "pelis.ini"
pelis_parser = ConfigParser.SafeConfigParser()
pelis_parser.read(pelis_ini)
      
patron_torrent = re.compile('.*<a href="(/torrent/[0-9]+/.*)">')
patron_torrent_id = re.compile('/torrent/([0-9]+)/.*')
patron_titulo_original = re.compile('.*tulo original[\s]*[:]*[\n]*[\s]*(.*)\r\n.*')
patron_titulo= re.compile('.*<h2>(.*)</h2>.*')
patron_titulo_parentesis = re.compile('(.*)\s\(.*\)')

#feed_url = pelis_parser.get("Config", "Feed")
db =  pelis_parser.get("Config", "DB")
torrent_dir = pelis_parser.get("Config", "Torrent_dir")
min_rating = float(pelis_parser.get("Config", "MinRating"))
sources_items = pelis_parser.items("Sources")
format_items = pelis_parser.items("Formats")

for key, source in sources_items:
    response = requests.get(source)
    if response.status_code == 200:
        iterator_torrent = patron_torrent.finditer(response.content)
        
        for torrent_group in iterator_torrent:
            torrent = torrent_group.group(1)
            response_torrent = requests.get('http://www.elitetorrent.net/' + torrent)
        
            if response_torrent.status_code == 200:            
                matcher_titulo = patron_titulo.search(response_torrent.content)
                
                if matcher_titulo != None:                
                    titulo_parentesis = matcher_titulo.group(1)
                    if any(formats in titulo_parentesis for key, formats in format_items):
                        #titulo_parentesis = Bajo un sol abrasador (Heatsroke) (HDRip)
                        matcher_titulo_parentesis = patron_titulo_parentesis.search(titulo_parentesis)
                        if matcher_titulo_parentesis != None:
                            titulo = matcher_titulo_parentesis.group(1)
                        else:
                            titulo = titulo_parentesis
                        #titulo = Bajo un sol abrasador (Heatsroke)
                        
                        matcher_titulo_original = patron_titulo_original.search(response_torrent.content)
                        if matcher_titulo_original != None:  
                            titulo_original = matcher_titulo_original.group(1)
                            matcher_titulo_parentesis = patron_titulo_parentesis.search(titulo_original)
                            if matcher_titulo_parentesis != None:
                                titulo_original = matcher_titulo_parentesis.group(1)
                        else:
                            titulo_original = titulo    
                                        
                        if not existInBd(titulo):
                            ia=imdb.IMDb()       
                            #print "Searching in IMBd : " + titulo_original + " (" + titulo + ")"          
                            s_result=ia.search_movie(titulo_original)
                            if len(s_result) > 0:
                                item = s_result[0] 
                                ia.update(item)
                                if 'rating' in item.keys():
                                    rating = float(item['rating']);
                                    
                                    if rating > min_rating:
                                        #/torrent/25359/appleseed-alpha-hdrip
                                        matcher_torrent_id = patron_torrent_id.search(torrent)
                                        if matcher_torrent_id != None:
                                            torrent_id = matcher_torrent_id.group(1)
                                            response = requests.get('http://www.elitetorrent.net/get-torrent/' + torrent_id)
                                            if response.status_code == 200:
                                                f = open(torrent_dir + titulo_parentesis.translate(None, '¿?!@#$') + "#" + str(rating) + ".torrent" , 'wb')
                                                f.write(response.content)
                                                f.close()
                                                writeInBd(titulo, rating)
                                                print "Adding (#" + str(rating) + ") : " + titulo_parentesis
                                            else:
                                                print "Not responding " + 'http://www.elitetorrent.net/get-torrent/' + torrent_id
                                        else:
                                            print "Bad torrent link " + torrent_id
                                    else:
                                        print "Low Rating (#" + str(rating) + ") : " + titulo_parentesis
                                        writeInBd(titulo, rating)
                                else:
                                    print "Pelicula no encontrada : " + titulo_parentesis
                            else:
                                print "Rating not found : " + titulo_parentesis
                    else:
                        print "Low format " + titulo_parentesis
                else:
                    print "Incorrect Title" + torrent
            else:
                print "Not responding " + torrent
    else:
        print "Not responding " + source


