# -*- coding: utf-8 -*-
from plexapi.server import PlexServer
import datetime
import configparser
import os
import transmissionrpc
import shutil
import time
import json


def report(action, type, element, detail):
    payload = {}
    payload['id'] = "purge"
    payload['date'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
    payload['action'] = action
    payload['type'] = type
    payload['element'] = element
    payload['detail'] = detail          
    return json.dumps(payload)   

debug = False

passwd_ini = "./passwd.ini"
passwd_parser = configparser.ConfigParser()
passwd_parser.read(passwd_ini)
passwd = passwd_parser.sections()

config_ini = "./config.ini"
config_parser = configparser.ConfigParser()
config_parser.read(config_ini)
config = config_parser.sections()
path_config = config_parser.get("Feeder", "Path_config")

series_ini = path_config + "series.ini"
series_parser = configparser.ConfigParser()
series_parser.read(series_ini) 

plex_token = passwd_parser.get("Plex", "token")
plex_url = config_parser.get("Plex", "plex_url")
plex = PlexServer(plex_url, plex_token) 

transmission_ip = config_parser.get("Transmission", "ip")
transmission_port = config_parser.get("Transmission", "port")
transmission_user = passwd_parser.get("Transmission", "user")
transmission_password = passwd_parser.get("Transmission", "password")

transmission = transmissionrpc.Client(transmission_ip, transmission_port, user = transmission_user, password = transmission_password)

# Purge torrent without data.
# Generate list of completed torrents already shared
torrents = transmission.get_torrents()
torrent_delete_candidates={}
torrent_not_delete_candidates={}
for torrent in torrents:
    torrent_id = torrent.__getattr__('id')
    if torrent.__getattr__('error') == 3:
        print (report("warn", "torrent", str(torrent), "Deleting torrent due to No Data Found "))
        if not debug:
            transmission.remove_torrent(torrent_id, True)
    else:        
        if torrent.status == 'stopped':
            torrent_delete_candidates[torrent.__getattr__('name')] = torrent_id
        else:
            torrent_not_delete_candidates[torrent.__getattr__('name')] = torrent_id              
       
#generate show purge list
torrent_delete={}
path_delete=[]
section = plex.library.section('Series de TV')
for show in section.all():
    for episode in show.episodes():
        if  episode.isWatched: 
            elapsed = datetime.datetime.now() - episode.lastViewedAt
            if elapsed > datetime.timedelta(days=3):
                # En caso que hayamos visto el episodio hace 3 días o más
                location = episode.locations[0]
                path_name =  os.path.basename(os.path.dirname(os.path.normpath(location)))
                if path_name in torrent_delete_candidates:
                    # En caso que sea un torrent ya compartido, lo borramos desde el torrent
                    print (report("warn", "episode", show.title + ": " + episode.title, "watched episode"))
                    torrent_delete[path_name] = torrent_delete_candidates[path_name]
                elif path_name not in torrent_not_delete_candidates:
                    # En caso que no se encuentre en la lista de torrents, entonces es un fichero fuera del torrent y se debe eliminar el directorio.
                    print (report("warn", "episode", show.title + ": " + episode.title, "watched episode"))
                    path_delete.append(path_name)
            else:
                timeToDelete = datetime.timedelta(days=3) - elapsed
                print (report("info", "episode", show.title + ": " + episode.title, "will be deleted in " + str(timeToDelete)))

torrent_complete_dir = config_parser.get("Feeder", "Torrent_complete_dir") 
torrent_dest_dir = config_parser.get("Feeder", "Torrent_dir")

for torrent_dir in torrent_delete.keys():
    if not debug:
        transmission.remove_torrent(torrent_delete[torrent_dir], True)    

for path in path_delete:
    if not debug:  
        shutil.rmtree(torrent_complete_dir + path, ignore_errors=True)

update = False

#purge series.ini
for section in series_parser.sections():   
    inrss = series_parser.get(section, 'inrss')
    torrent_dir = series_parser.get(section, 'torrent_dir')     
    if inrss == 'false': #En caso que el episodio no esté en el feed
        if not os.path.exists(torrent_complete_dir + torrent_dir) or torrent_dir in torrent_delete or torrent_dir in path_delete:        
            series_parser.remove_section(section)        
            update = True
            print (report("warn", "section", section, "deleted section (torrent directory does not exist and out of RSS)"))

if update:
    if not debug:
        with open(series_ini, 'w') as configfile:
            series_parser.write(configfile)   