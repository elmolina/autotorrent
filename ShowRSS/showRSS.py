# -*- coding: utf-8 -*-
import configparser
import feedparser
import re
import os
import datetime
import shutil
import time
import transmissionrpc
import json

def report(action, type, element, detail):
    payload = {}
    payload['id'] = "showRSS"
    payload['date'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
    payload['action'] = action
    payload['type'] = type
    payload['element'] = element
    payload['detail'] = detail          
    return json.dumps(payload)   

config_ini = "config.ini"
config_parser = configparser.ConfigParser()
config_parser.read(config_ini)

passwd_ini = "passwd.ini"
passwd_parser = configparser.ConfigParser()
passwd_parser.read(passwd_ini)
passwd = passwd_parser.sections()

torrent_complete_dir = config_parser.get("Feeder", "Torrent_complete_dir")
torrent__dir = config_parser.get("Feeder", "Torrent_dir")

feed_url = config_parser.get("Feeder", "Feed_url")
feed = feedparser.parse(feed_url)

path_config = config_parser.get("Feeder", "Path_config")

series_ini_file = path_config + "series.ini"
series_ini = configparser.ConfigParser()
series_ini.read(series_ini_file)

transmission_ip = config_parser.get("Transmission", "ip")
transmission_port = config_parser.get("Transmission", "port")
transmission_user = passwd_parser.get("Transmission", "user")
transmission_password = passwd_parser.get("Transmission", "password")

transmission = transmissionrpc.Client(transmission_ip, transmission_port, user = transmission_user, password = transmission_password)

# Marcar todas como fuera del RSS. Las series que ya no estén en el RSS se marcaran a inRSS a false para ser purgadas del fichero de configuración series.ini
if len(feed.entries) > 0:
    for section in series_ini.sections():
        series_ini.set(section, 'inRSS', 'false')

re_season_episode = re.compile('S([0-9]{2})E([0-9]{2})')
re_season_episode_repack = re.compile('(PROPER|REPACK)')
re_version_subtitulo = re.compile('(WEB|KILLERS|DIMENSION)')

for entry in feed.entries:            
    matcher_patron_season_episode = re_season_episode.search(entry.title)
    if matcher_patron_season_episode != None:    
        # Determinar nombre de la sección
        season =  matcher_patron_season_episode.group(1)
        episode = matcher_patron_season_episode.group(2)       
        section = entry.tv_show_name + ' S' + season + 'E' + episode

        found_repack = False
        matcher_repack = re_season_episode_repack.search(entry.title)
        # Borramos las verisones no proper/repack y las sustituimos por las proper/repack
        if matcher_repack != None:
            if series_ini.has_section(section):
                torrent_dir = series_ini.get(section, 'torrent_dir')
                # Esperamos a asegurarnos que se haya bajado el torrent
                print(report("info", "section", section, "REPACK/PROPER torrent found"))
                # Borramos el torrent
                if os.path.exists(torrent_complete_dir + torrent_dir):                    
                    shutil.rmtree(torrent_complete_dir + torrent_dir)  
                # Borramos la entrada en el fichero de configuración
                series_ini.remove_section(section)        
            section = section + " " + matcher_repack.group(1)
        else:
            # En caso que el feed actual no sea REPACK, buscamos si existe en la configuración un Repack para poder ignorarlo    
            re_section_with_repack = re.compile(re.escape(section) + " (PROPER|REPACK)")
            for s in series_ini.sections():
                found_repack = re_section_with_repack.search(s) != None
                if found_repack:
                    break

        if not found_repack:
            if not series_ini.has_section(section): 
                print (report("warn", "add", section, "new torrent found"))
                magnet_torrent = transmission.add_torrent(entry.link, paused = False)
                
                torrent_id = magnet_torrent.__getattr__('id')
                #transmission.queue_bottom(torrent_id)
                torrent = transmission.get_torrent(torrent_id)
                while len(torrent.__getattr__('files')) == 0:                    
                    torrent = transmission.get_torrent(torrent_id)
                    time.sleep(2)
                    
                transmission.queue_top(torrent_id)
                torrent_dir = torrent.__getattr__('name')
                    
                #obtener la versión del subtítulo
                version_subtitulo = ""
                matcher = re_version_subtitulo.search(torrent_dir)
                if matcher != None:
                    version_subtitulo = matcher.group(1)

                show_name = entry.tv_show_name

                #Guardar configuración
                series_ini.add_section(section)
                series_ini.set(section, 'show_name', show_name)
                series_ini.set(section, 'season', season)
                series_ini.set(section, 'episode', episode)
                series_ini.set(section, 'torrent_dir', torrent_dir)           
                series_ini.set(section, 'version_subtitulo', version_subtitulo)
                series_ini.set(section, 'inRSS', 'true')                                        
            else:
                series_ini.set(section, 'inRSS', 'true')
                print (report("info", "torrent", section, "already exists is feed"))
        else:
            print (report("info", "torrent", section, "Ignoring show due to REPACK/PROPER exists"))
    else:
        print (report("error", "torrent", entry.title, "Pattern SnnEnn not found un entry title"))

with open(series_ini_file, 'w') as configfile:
    series_ini.write(configfile)                                        