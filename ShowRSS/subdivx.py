# -*- coding: utf-8 -*-
import requests
import urllib3
import configparser
import sys
import re
import os
import datetime
import time
import zipfile
from io import BytesIO
import glob
import json
#import patoolib

def report(action, type, element, detail):
    payload = {}
    payload['id'] = "subdivx"
    payload['date'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
    payload['action'] = action
    payload['type'] = type
    payload['element'] = element
    payload['detail'] = detail          
    return json.dumps(payload)   

# Disable DH Key issues
#https://stackoverflow.com/questions/38015537/python-requests-exceptions-sslerror-dh-key-too-small
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
requests.packages.urllib3.disable_warnings()
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
try:
    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
except AttributeError:
    # no pyopenssl support used / needed / available
    pass


#from unrar import rarfile

#def extract_rar(path, file):
#    complete_path = os.path.join(path, file) 
#    with rarfile.RarFile(complete_path,"r") as rar_ref:
#        count = 0
#        for filename in rar_ref.namelist(): # Iteramos por todos los ficheros del zip
#            count += 1
#            target_name = os.path.splitext(file)[0] + "_" + str(count) + ".es.srt" # renombramos el zip a la extensión .es.srt
#            target_path = os.path.join(path, target_name)  # output path
#            with open(target_path, "wb") as f:  # open the output path for writing
#                f.write(rar_ref.read(filename))  # save the contents of the file in it              

#def extract_rar(path, file):
#    patoolib.extract_archive(os.path.join(path, file), outdir=path)

def extract_zip(path, file):
    complete_path = os.path.join(path, file) 
    with zipfile.ZipFile(complete_path,"r") as zip_ref:
        count = 0
        for filename in zip_ref.namelist(): # Iteramos por todos los ficheros del zip
            count += 1
            target_name = os.path.splitext(file)[0] + "_" + str(count) + ".es.srt" # renombramos el zip a la extensión .es.srt
            target_path = os.path.join(path, target_name)  # output path
            with open(target_path, "wb") as f:  # open the output path for writing
                f.write(zip_ref.read(filename))  # save the contents of the file in it           

config_ini = "./config.ini"
config_parser = configparser.ConfigParser()
config_parser.read(config_ini)
config = config_parser.sections()
torrent_complete_dir = config_parser.get("Feeder", "Torrent_complete_dir")
series_url = config_parser.get("Feeder", "Series_url")
path_config = config_parser.get("Feeder", "Path_config")

series_ini_file = path_config + "series.ini"
series_ini = configparser.ConfigParser()
series_ini.read(series_ini_file) 
                             
patron_subdivx_subs = re.compile(r'<a class="titulo_menu_izq" href="(http:\/\/www\.subdivx\.com\/.*?)">', re.DOTALL)
patron_file_subdivx_down = re.compile(r'http:\/\/www\.subdivx\.com\/bajar\.php\?id=(.*?)&u=.*', re.DOTALL)
patron_subdivx_down = re.compile(r'href="(http:\/\/www\.subdivx\.com\/bajar\.php\?id=.*?)"' , re.DOTALL)

update = False

for section in series_ini.sections():
    update_subtitle = True

    if series_ini.has_option(section, 'subdivx_updated'):
        subtitle_updated = series_ini.get(section, 'subdivx_updated')
        elapsed = datetime.datetime.now() - datetime.datetime.fromtimestamp(float(subtitle_updated)) 
        update_subtitle = elapsed > datetime.timedelta(hours=24)
        
    if update_subtitle:   
        show_name = series_ini.get(section, 'show_name')   
        season = series_ini.get(section, 'season')
        episode = series_ini.get(section, 'episode')
        torrent_dir = series_ini.get(section, 'torrent_dir')   

        if os.path.exists(torrent_complete_dir + torrent_dir): 
            # Realizamos la búsqueda del capítulo
            subdix_search_url = 'https://www.subdivx.com/index.php?buscar=' + (show_name) + ' S' + season + 'E' + episode + "&accion=5&masdesc=&subtitulos=1&realiza_b=1"
            response_subdix_search = requests.get(subdix_search_url, verify=False)            
            if response_subdix_search.status_code == 200:   
                matches_subdivx_subs = patron_subdivx_subs.findall(response_subdix_search.content.decode('iso-8859-1'))
                # Puede haber varias traducciones y se van añadiendo
                if len(matches_subdivx_subs) > 0:
                    for subdix_sub in matches_subdivx_subs:
                        response_subdix_sub = requests.get(subdix_sub, verify=False) 
                        if response_subdix_search.status_code == 200: 
                            # Obtenemos la URL del enlace a la descarga
                            matches_subdivx_down = patron_subdivx_down.findall(response_subdix_sub.content.decode('iso-8859-1'))
                            if matches_subdivx_down != None and len(matches_subdivx_down) > 0:
                                subdivx_down = matches_subdivx_down[0]
                                # En el patrón de la URL se encuentra el nombre del fichero. Revisamos que no lo tengamos ya bajado     
                                matches_file_subdivx_down = patron_file_subdivx_down.findall(subdivx_down)
                                if matches_file_subdivx_down != None and len(matches_file_subdivx_down) > 0:
                                    file_subdivx_down = matches_file_subdivx_down[0]  # Obtenemos el nombre del fichero
                                    # En caso que el fichero no lo tengamos en el directorio del torrent
                                    glob_path = os.path.join(torrent_complete_dir, torrent_dir, file_subdivx_down)                                
                                    glob_path = glob.escape(glob_path) + ".*"
                                    if not glob.glob(glob_path):                                   
                                        response_subdivx_down = requests.get(subdivx_down, verify=False)
                                        if response_subdivx_down.status_code == 200:
                                            # Obtenemos el nombre del zip sin path. Viene en un atributo llamado url
                                            zip_file = os.path.split(response_subdivx_down.url)[1]
                                            # Guardamos el zip en el directorio
                                            zip_path = os.path.join(torrent_complete_dir, torrent_dir, zip_file)
                                            f = open(zip_path, 'wb')
                                            f.write(response_subdivx_down.content)
                                            f.close()    

                                            if zipfile.is_zipfile(zip_path): # En caso que sea zip
                                                extract_zip (os.path.join(torrent_complete_dir, torrent_dir), zip_file)                                                          
                                                series_ini.set(section, "subdivx_updated", str(time.time()))                                          
                                                update = True         
                                                print(report("warn", "subtitle", section, "New subtitle added: " + zip_file))                                   
                                            #elif rarfile.is_rarfile(zip_path):
                                            #    print("s")
                                            #else: 
                                            #    extract_rar (os.path.join(torrent_complete_dir, torrent_dir), zip_file)                                                      
                                            #    series_ini.set(section, "subdivx_updated", str(time.time()))                                          
                                            #    update = True                                            
                                            #    print ("[INFO] Added subtitle '" + subdix_sub + "' of '" + section + "'")
                                            else:                                                       
                                                print(report("info", "subtitle", section, "Wrong format in compressed file: " + zip_file))                                
                                        else:
                                            print(report("error", "subtitle",  section, " HTTP error code '" + str(response_subdivx_down.status_code) + "' getting '" + subdivx_down))
                                    else:
                                        if not series_ini.has_option(section, "subdivx_updated"):
                                            series_ini.set(section, "subdivx_updated", str(time.time()))                                          
                                            update = True                                            
                                        print(report("info", "subtitle", section, "Subtitle already exist '" + file_subdivx_down))                                   
                                else:
                                    print(report("error", "subtitle", section, "No 'subdivx_file' found in '" + subdivx_down))                                
                            else: 
                                print(report("error", "subtitle", section, "No 'http://www.subdivx.com/bajar.php' found in '" + subdix_sub))                                
                        else:
                            print(report("error", "subtitle", section, "HTTP error code '" + str(response_subdix_search.status_code) + "' getting '" + subdix_search_url))                                
                else:
                    print(report("info", "subtitle", section, "Subtitle not found for '" + subdix_search_url))                                
            else:
                print(report("error", "subtitle", section, "HTTP error code '" + str(response_subdix_search.status_code) + "' getting '" + subdix_search_url))                                
        else:
            print(report("info", "subtitle", section, "Subtitle not processed. The show downloading is in progress or is deleted"))                                
if update:
    with open(series_ini_file, 'w') as configfile:
        series_ini.write(configfile)  