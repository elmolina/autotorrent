# -*- coding: utf-8 -*-
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import configparser
import sys
import re
import os
import datetime
import time

print ('# ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

config_ini = "config.ini"
config_parser = configparser.ConfigParser()
config_parser.read(config_ini)
config = config_parser.sections()
torrent_complete_dir = config_parser.get("Feeder", "Torrent_complete_dir")
series_url = config_parser.get("Feeder", "Series_url")
path_config = config_parser.get("Feeder", "Path_config")

series_ini_file = path_config + "series.ini"
series_ini = configparser.ConfigParser()
series_ini.read(series_ini_file) 

patron_episodios = re.compile('<a href=\'(//www.tusubtitulo.com/episodes/.*?)\'>', re.DOTALL)
patron_subtitulo = re.compile('<ul class=\"sslist\">.*?Español.*?<a href="((?:original|updated)/[0-9/]+)">.*?</ul>', re.DOTALL)

update = False

try:
    content = "None"
    
    for section in series_ini.sections():
        if not series_ini.has_option(section, "tussubtitulos_id"):        
            
            show_name = series_ini.get(section, "show_name")     
            
            if content == "None":
                response_tusubtitulo = requests.get(series_url, verify=False)
                if response_tusubtitulo.status_code == 200:
                    content = response_tusubtitulo.text    
                else:
                    print ("[ERROR] HTTP error code '" + response_tusubtitulo.status_code + "' getting '" + show_name + "'")
            
            re_show_id = re.compile('\"/show/([0-9]+)\">' + re.escape(show_name))
            matcher_show_id = re_show_id.search(content)
            if matcher_show_id != None:
                    tussubtitulos_id = matcher_show_id.group(1)            
                    series_ini.set(section, "tussubtitulos_id", tussubtitulos_id)
                    update = True
            else:
                print ("[ERROR] Not finded subtitle id of '" + show_name + "'")
except:
    print ("[ERROR] Unexpected error getting '" + series_url, sys.exc_info()[0] + "'")

for section in series_ini.sections():
    if series_ini.has_option(section, 'tussubtitulos_id'):
        update_subtitle = True
                
        if series_ini.has_option(section, 'tussubtitulos_updated'):
            tussubtitulos_updated = series_ini.get(section, 'tussubtitulos_updated')
            elapsed = datetime.datetime.now() - datetime.datetime.fromtimestamp(float(tussubtitulos_updated)) 
            update_subtitle = elapsed > datetime.timedelta(hours=24)
            
        if update_subtitle:   
            show_name = series_ini.get(section, 'show_name')   
            season = int(series_ini.get(section, 'season'))
            episode = int(series_ini.get(section, 'episode'))
            torrent_dir = series_ini.get(section, 'torrent_dir')   
            tussubtitulos_id = series_ini.get(section, 'tussubtitulos_id')
            if os.path.exists(torrent_complete_dir + torrent_dir):            
                tusubtitulo_season_url = 'https://www.tusubtitulo.com/ajax_loadShow.php?show=' + str(tussubtitulos_id) + '&season=' + str(season)
                response_tusubtitulo_season = requests.get(tusubtitulo_season_url, verify=False)            
                if response_tusubtitulo_season.status_code == 200:   
                    episodios_urls = patron_episodios.findall(response_tusubtitulo_season.content.decode('utf-8'))
                    episodio_url = next((x for x in episodios_urls if x.find(str(season) + "x" + str(episode).zfill(2)) != -1), None)                    
                    if episodio_url != None:
                        seasson_episode_url = "https:" + episodio_url
                        response_tusubtitulo_episode = requests.get(seasson_episode_url, verify=False)  
                        if response_tusubtitulo_episode.status_code == 200:   
                            subtitulo_urls = patron_subtitulo.findall(response_tusubtitulo_episode.content.decode('utf-8'))
                            if len(subtitulo_urls) > 0:
                                for i, subtitulo_url in enumerate(subtitulo_urls): 
                                    subtitulo_url = "https://www.tusubtitulo.com/" + subtitulo_url                                      
                                    subtitulo_file = subtitulo_url.replace('/', '_')
                                    subtitulo_file = subtitulo_file.replace(':', '_')
                                    subtitulo_file = section + subtitulo_file + '.es.srt'
                                    subtitulo_file_complete = torrent_complete_dir + torrent_dir + '/' + subtitulo_file                                        
                                    if not os.path.exists(subtitulo_file_complete): 
                                        s = requests.Session()                                            
                                        response = s.get(subtitulo_url, headers={'Referer': seasson_episode_url}, verify=False)
                                        if response.status_code == 200:
                                            f = open(subtitulo_file_complete, 'wb')
                                            f.write(response.content)
                                            f.close()
                                                
                                            series_ini.set(section, "tussubtitulos_updated", str(time.time()))
                                                
                                            update = True
                                                
                                            print ("[INFO] Added subtitle '" + subtitulo_url + "' of '" + section + "'")
                                    else:
                                        update = True
                                        series_ini.set(section, "tussubtitulos_updated", str(time.time()))
                            else:
                                print ("[WARN] Subtitle not available for '" + seasson_episode_url + "' of show '" + section + "'")
                        else:
                            print ("[ERROR] HTTP error code '" + str(response_tusubtitulo_episode.status_code) + "' getting '" + seasson_episode_url + "' of show '" + section + "'")                        
                    else:
                        print ("[ERROR] Cannot find subtitle of '" + show_name + "' Seasson '" + str(season) + "' Episode '" + str(episode) + "' URL='" + tusubtitulo_season_url + "'")
                else:
                    print ("[ERROR] HTTP error code '" + str(response_tusubtitulo_season.status_code) + "' getting '" + tusubtitulo_season_url + "' of show '" + section + "'")
            else:
                print ("[WARN] Subtitle not processed. The show downloading is in progress or is deleted: '" + section + "'")
    else:
        print ("[ERROR] Malformed config for section '" + section + "'")

if update:
    with open(series_ini_file, 'w') as configfile:
        series_ini.write(configfile)     
